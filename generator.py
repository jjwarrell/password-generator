import random
import string



def makePassword(len, use_lower, use_upper, use_num, use_symbols):
    lower = ""
    upper = ""
    num = ""
    symbols = ""


    if(use_lower == True):
        lower = string.ascii_lowercase
    if(use_upper == True):
        upper = string.ascii_uppercase
    if(use_num == True):
        num = string.digits
    if(use_symbols == True):
        symbols = string.punctuation

    all = lower + upper + num + symbols

    temp = random.sample(all, int(len))

    password = "".join(temp)

    return password

