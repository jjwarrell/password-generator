from flask import Flask, request, send_from_directory, render_template, redirect
from flask.helpers import send_file
from generator import *


app = Flask(__name__)

@app.route("/")
def index():
    return send_file("index.html")

@app.route('/password/', methods=['post', 'get'])
def password():
    if request.method == 'POST':
        length = request.form.get('length')
        upper = request.form.get('upper')
        lower = request.form.get('lower')
        num = request.form.get('num')
        symbols = request.form.get('symbols')



        if(upper == "on"):
            upper = True
        else:
            upper = False

        if(lower == "on"):
            lower = True
        else:
            lower = False

        if(num == "on"):
            num = True
        else:
            num = False

        if(symbols == "on"):
            symbols = True
        else:
            symbols = False
        
        password = makePassword(length, lower, upper, num, symbols)
        return render_template('password.html', password=password)
    else:
        return redirect("/", code=500)
